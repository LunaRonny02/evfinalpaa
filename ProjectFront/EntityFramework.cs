using ProjectFront.Models;
using System.Data.Entity;

namespace ProjectFront
{
    public class EntityFramework : DbContext
    {
        // Your context has been configured to use a 'EntityFramework' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ProjectFront.EntityFramework' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'EntityFramework' 
        // connection string in the application configuration file.
        public EntityFramework()
            : base("name=EntityFramework")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.
        public DbSet<User> Users { get; set; }
        public DbSet<League> Leagues { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Player> Players { get; set; }
        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}