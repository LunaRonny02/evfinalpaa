﻿using ProjectFront.Models;
using System;

namespace ProjectFront.Pages
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void BtnRegisterClick(object sender, EventArgs e)
        {
            using (var db = new EntityFramework())
            {
                db.Users.Add(new User { userName = tbUser.Text.Trim(), userPass = tbPass.Text.Trim(), userType="basico"});
                db.SaveChanges();
            }

            Response.Redirect("Index.aspx");
        }
    }
}