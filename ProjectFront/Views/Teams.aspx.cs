﻿using System;
using System.Data;
using System.Data.Entity;
using System.Web.UI.WebControls;

namespace ProjectFront.Pages
{
    public partial class Teams : System.Web.UI.Page
    {
        public static string sId = "-1";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["leagueId"] != null)
                {
                    sId = Request.QueryString["leagueId"].ToString();
                    CargarTabla();
                }
            }

            string[] leagueId = sId.Split('-');

            if (Session["usuarioLogueado"] != null)
            {
                string[] userId = Session["usuarioLogueado"].ToString().Split('-');

                if (leagueId[0].Trim() == userId[1].Trim())
                {
                    BtnCreate.Visible = true;

                    foreach (GridViewRow row in gvEquipos.Rows)
                    {
                        Button btn1 = row.FindControl("btnDelete") as Button;
                        Button btn2 = row.FindControl("BtnUpdate") as Button;

                        btn1.Visible = true;
                        btn2.Visible = true;
                    }
                }
            }else{

                BtnCreate.Visible = false;
            }
        }

        void CargarTabla()
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("Id", typeof(string));
            dt.Columns.Add("Logo", typeof(string));
            dt.Columns.Add("Nombre", typeof(string));

            using (var db = new EntityFramework())
            {
                string[] leagueId = sId.Split('-');

                foreach (var team in db.Teams)
                {
                    if (Int32.Parse(leagueId[1]) == team.team_LeagueId) {

                        dt.Rows.Add(new Object[] { sId + "-" + team.teamId.ToString()
                        , team.teamImg, team.teamName });
                    }
                }
            }
            gvEquipos.DataSource = dt;
            gvEquipos.DataBind();
        }
        protected void BtnCreateClick(object sender, EventArgs e)
        {

            Response.Redirect("~/Views/CRUD.aspx?op=C&op2="+ sId);
        }
        protected void BtnReadClick(object sender, EventArgs e)
        {
            string id;
            Button BtnConsultar = (Button)sender;
            GridViewRow selectedrow = (GridViewRow)BtnConsultar.NamingContainer;
            id = selectedrow.Cells[1].Text;
            Response.Redirect("~/Views/Players.aspx?teamId=" + id + "&op=R");
        }
        protected void BtnUpdateClick(object sender, EventArgs e)
        {
            string id;
            Button BtnConsultar = (Button)sender;
            GridViewRow selectedrow = (GridViewRow)BtnConsultar.NamingContainer;
            id = selectedrow.Cells[1].Text;
            Response.Redirect("~/Views/CRUD.aspx?id=" + id + "&op=U" + "&op2=U");
        }
        protected void BtnDeleteClick(object sender, EventArgs e)
        {
            Button BtnConsultar = (Button)sender;
            GridViewRow selectedrow = (GridViewRow)BtnConsultar.NamingContainer;
            string[] ids = selectedrow.Cells[1].Text.Split('-');
            using (var db = new EntityFramework())
            {
                var team = db.Teams.Find(Int32.Parse(ids[2]));
                db.Entry(team).State = EntityState.Deleted;
                db.SaveChanges();
            }
            Response.Redirect(Request.RawUrl);

            //Response.Redirect("~/Views/CRUD.aspx?id=" + id + "&op=D");
        }
    }
}