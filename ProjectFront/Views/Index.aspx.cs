﻿using System;
using System.Data;
using System.Data.Entity;
using System.Web.UI.WebControls;

namespace ProjectFront.Pages
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            CargarTabla();
            if (Session["usuarioLogueado"] != null)
            {
                BtnCreate.Visible = true;

                string[] userId = Session["usuarioLogueado"].ToString().Split('-');

                foreach (GridViewRow row in gvLigas.Rows)
                {
                    Button btn1 = row.FindControl("btnDelete") as Button;
                    Button btn2 = row.FindControl("BtnUpdate") as Button;
                    string[] leagueId = row.Cells[1].Text.Split('-');

                    if (leagueId[0].Trim() == userId[1].Trim())
                    {
                        btn1.Visible = true;
                        btn2.Visible = true;

                    }
                    else
                    {
                        btn1.Visible = false;
                        btn2.Visible = false;
                    }
                }
            }
            else
            {

                BtnCreate.Visible = false;
            }
        }

        void CargarTabla()
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("Id", typeof(string));
            dt.Columns.Add("Logo", typeof(string));
            dt.Columns.Add("Nombre", typeof(string));
            using (var db = new EntityFramework())
            {

                foreach (var league in db.Leagues)
                {
                    dt.Rows.Add(new Object[] { league.league_UserId.ToString() + "-" + league.leagueId.ToString(), league.leagueImg, league.leagueName });
                }
            }
            gvLigas.DataSource = dt;
            gvLigas.DataBind();

        }
        protected void BtnCreateClick(object sender, EventArgs e)
        {
            Response.Redirect("~/Views/CRUD.aspx?op=C");
        }
        protected void BtnReadClick(object sender, EventArgs e)
        {
            string id;
            Button BtnConsultar = (Button)sender;
            GridViewRow selectedrow = (GridViewRow)BtnConsultar.NamingContainer;
            id = selectedrow.Cells[1].Text;
            Response.Redirect("~/Views/Teams.aspx?leagueId=" + id);
        }
        protected void BtnUpdateClick(object sender, EventArgs e)
        {
            string id;
            Button BtnConsultar = (Button)sender;
            GridViewRow selectedrow = (GridViewRow)BtnConsultar.NamingContainer;
            id = selectedrow.Cells[1].Text;
            Response.Redirect("~/Views/CRUD.aspx?id=" + id + "&op=U");
        }
        protected void BtnDeleteClick(object sender, EventArgs e)
        {

            Button BtnConsultar = (Button)sender;
            GridViewRow selectedrow = (GridViewRow)BtnConsultar.NamingContainer;
            string[] ids = selectedrow.Cells[1].Text.Split('-');
            using (var db = new EntityFramework())
            {
                var league = db.Leagues.Find(Int32.Parse(ids[1]));
                db.Entry(league).State = EntityState.Deleted;
                db.SaveChanges();
            }
            Response.Redirect(Request.RawUrl);

            //Response.Redirect("~/Views/CRUD.aspx?id=" + id + "&op=D");
        }
    }
}