﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.Master" AutoEventWireup="true" CodeBehind="Players.aspx.cs" Inherits="ProjectFront.Views.Players" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
        <form runat="server">
        <br />
        <div class="d-flex justify-content-center">
            <h2>Listado de Jugadores</h2>
        </div>
        <br />
        <div class="container">
            <div class="row">
                <div class="d-flex justify-content-center">
                    <asp:Button runat="server" ID="BtnCreate" CssClass="btn btn-success form-control-sm" Text="Create new Player" Visible="false" OnClick="BtnCreateClick"/>
                </div>
            </div>
        </div>
        <br />
        <div class="row justify-content-center">
            <div class="col-md-auto">
                <asp:GridView runat="server" ID="gvJugadores" class="table table-borderless table-hover">
                    <Columns>
                        <asp:TemplateField HeaderText="Opciones"> 
                            <ItemTemplate>
                                <asp:Button runat="server" Text="Update" CssClass="btn form-control-sm btn-warning" ID="BtnUpdate" Visible="false" OnClick="BtnUpdateClick"/>
                                <asp:Button runat="server" Text="Delete" CssClass="btn form-control-sm btn-danger" ID="BtnDelete" Visible="false" OnClick="BtnDeleteClick"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </form>
</asp:Content>
