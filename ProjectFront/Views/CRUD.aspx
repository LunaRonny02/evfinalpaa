﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.Master" AutoEventWireup="true" CodeBehind="CRUD.aspx.cs" Inherits="ProjectFront.Pages.CRUD" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <br />
    <div class="d-flex justify-content-center mb-5">
        <asp:Label runat="server" CssClass="h2" ID="lbltitulo"></asp:Label>
    </div>
    <form runat="server" class="h-100 d-flex align-items-center justify-content-center">
        <div>
            <div class="row justify-content-center mb-3">
                <div class="col-6">
                    <img runat="server" id="imgCrud" class="rounded-circle" src="~/Content/login.jpg" width="160" height="160">
                </div>
            </div>
            <div class="mb-3">
                <label class="form-label">Nombre</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="tbNombre"></asp:TextBox>
            </div>
            <div class="mb-3">
                <asp:Label runat="server" ID="lbLastname" class="form-label" Visible="false">Apellido</asp:Label>
                <asp:TextBox runat="server" CssClass="form-control" ID="tbLastname" Visible="false"></asp:TextBox>
            </div>
            <div class="mb-3">
                <asp:Label runat="server" ID="lbDate" class="form-label" Visible="false">Fecha de nacimiento</asp:Label>
                <asp:TextBox runat="server" TextMode="Date" CssClass="form-control" ID="tbDate" Visible="false"></asp:TextBox>
            </div>
            <div class="col">
                <div>
                    <asp:Label id="lbImg" runat="server"></asp:Label>   
                    <asp:FileUpload id="FileUpload" runat="server"></asp:FileUpload>
                    <br /><br /> 
                    <hr />
                </div>
            </div>
            <asp:Button runat="server" CssClass="btn btn-success" ID="BtnCreate" Text="Create" Visible="false" OnClick="BtnCreateClick"/>
            <asp:Button runat="server" CssClass="btn btn-primary" ID="BtnUpdate" Text="Update" Visible="false" OnClick="BtnUpdateClick"/>
            <asp:Button id="backButton" runat="server" CssClass="btn btn-dark" Text="Volver" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:Button>
        </div>
    </form>
</asp:Content>
