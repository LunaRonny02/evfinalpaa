﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="ProjectFront.Pages.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <br />
    <div class="d-flex align-items-center justify-content-center mb-5">
        <asp:Label runat="server" CssClass="h2" ID="lblTitulo">Registrar nuevo usuario</asp:Label>
    </div>
    <form runat="server" class="h-100 d-flex align-items-center justify-content-center">
        <div>
            <div class="mb-3">
                <label class="form-label">Usuario</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="tbUser"></asp:TextBox>
            </div>
            <div class="mb-3">
                <label class="form-label">Contraseña</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="tbPass" TextMode="Password"></asp:TextBox>
            </div>
            <asp:Button runat="server" CssClass="btn btn-primary" ID="BtnRegister" Text="Register" Visible="true" OnClick="BtnRegisterClick"/>
            <asp:Button id="backButton" runat="server" CssClass="btn btn-dark" Text="Volver" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:Button>
        </div>
    </form>
</asp:Content>