﻿using System;
using System.Data;
using System.Data.Entity;
using System.Web.UI.WebControls;

namespace ProjectFront.Views
{
    public partial class Players : System.Web.UI.Page
    {
        public static string sId = "-1";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["teamId"] != null)
                {
                    sId = Request.QueryString["teamId"].ToString();
                    CargarTabla();
                }
            }

            string[] teamId = sId.Split('-');

            if (Session["usuarioLogueado"] != null)
            {
                string[] userId = Session["usuarioLogueado"].ToString().Split('-');

                if (teamId[0].Trim() == userId[1].Trim())
                {
                    BtnCreate.Visible = true;

                    foreach (GridViewRow row in gvJugadores.Rows)
                    {
                        Button btn1 = row.FindControl("btnDelete") as Button;
                        Button btn2 = row.FindControl("BtnUpdate") as Button;

                        btn1.Visible = true;
                        btn2.Visible = true;
                    }
                }
            }
            else
            {

                BtnCreate.Visible = false;
            }
        }
        void CargarTabla()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Id", typeof(string));
            dt.Columns.Add("Logo", typeof(string));
            dt.Columns.Add("Nombre", typeof(string));
            dt.Columns.Add("Apellido", typeof(string));
            dt.Columns.Add("Fecha Nacimiento", typeof(string));

            using (var db = new EntityFramework())
            {
                string[] teamId = sId.Split('-');
                String imgDataURL = null;
                foreach (var player in db.Players)
                {
                    string[] date = player.playerBirthdate.ToString().Split(' ');
                    if (Int32.Parse(teamId[2]) == player.player_TeamId)
                    {
                        if (player.playerImg != null)
                        {
                            string imreBase64Data = Convert.ToBase64String(player.playerImg);
                            imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                            
                        }
                        dt.Rows.Add(new Object[] { sId + "-" + player.playerId.ToString(), player.playerImg, player.playerName, 
                            player.playerLastName, date[0] });
                    }
                }
            }
            gvJugadores.DataSource = dt;
            gvJugadores.DataBind();

        }

        protected void BtnCreateClick(object sender, EventArgs e)
        {

            Response.Redirect("~/Views/CRUD.aspx?op=C&op3=" + sId);
        }

        protected void BtnUpdateClick(object sender, EventArgs e)
        {

            Button BtnConsultar = (Button)sender;
            GridViewRow selectedrow = (GridViewRow)BtnConsultar.NamingContainer;
            string id = selectedrow.Cells[1].Text;
            Response.Redirect("~/Views/CRUD.aspx?id=" + id + "&op=U"+ "&op3=U");
        }

        protected void BtnDeleteClick(object sender, EventArgs e)
        {
            Button BtnConsultar = (Button)sender;
            GridViewRow selectedrow = (GridViewRow)BtnConsultar.NamingContainer;
            string[] ids = selectedrow.Cells[1].Text.Split('-');
            using (var db = new EntityFramework())
            {
                var player = db.Players.Find(Int32.Parse(ids[3]));
                db.Entry(player).State = EntityState.Deleted;
                db.SaveChanges();
            }
            Response.Redirect(Request.RawUrl);
            //Response.Redirect("~/Views/CRUD.aspx?id=" + id + "&op=D");
        }
    }
}