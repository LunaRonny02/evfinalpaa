﻿using ProjectFront.Models;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace ProjectFront.Pages
{
    public partial class CRUD : System.Web.UI.Page
    {
        public static string sID = "-1";
        public static string sOpc = "";
        public static string sOpc2 = "";
        public static string sOpc3 = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    sID = Request.QueryString["id"].ToString();
                    
                    CargarDatos();
                    //
                }

                if (Request.QueryString["op"] != null)
                {
                    sOpc = Request.QueryString["op"].ToString();

                    switch (sOpc)
                    {
                        case "C":
                            if (Request.QueryString["op2"] != null)
                            {
                                this.lbltitulo.Text = "Ingresar nuevo Equipo";

                            } else if (Request.QueryString["op3"] != null) {

                                this.lbltitulo.Text = "Ingresar nuevo Jugador";
                                lbLastname.Visible = true;
                                tbLastname.Visible = true;
                                tbDate.Visible = true;
                                lbDate.Visible = true;
                            }
                            else
                            {
                                this.lbltitulo.Text = "Ingresar nueva Liga";
                            }
                            this.BtnCreate.Visible = true;
                            break;

                        case "R":
                            break;
                        case "U":

                            if (Request.QueryString["op2"] != null)
                            {
                                this.lbltitulo.Text = "Modificar Equipo";

                            }
                            else if (Request.QueryString["op3"] != null)
                            {

                                this.lbltitulo.Text = "Modificar Jugador";
                                tbDate.Visible= true;
                                lbDate.Visible = true;
                                tbLastname.Visible= true;
                                tbDate.TextMode = TextBoxMode.DateTime;
                            }
                            else
                            {
                                this.lbltitulo.Text = "Modificar Liga";
                            }
                            this.BtnUpdate.Visible = true;
                            break;

                        case "D":
                            this.lbltitulo.Text = "Eliminar usuario";
                            break;
                    }
                }
            }
        }

        void CargarDatos()
        {
            
            string[] ids = sID.Split('-');

            if (Request.QueryString["op2"] != null)
            {
                using (var db = new EntityFramework())
                {
                    var team = db.Teams.Find(Int32.Parse(ids[2]));
                    tbNombre.Text = team.teamName;
                    if (team.teamImg != null)
                    {
                        string imreBase64Data = Convert.ToBase64String(team.teamImg);
                        string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                        imgCrud.Src = imgDataURL;
                    }

                }

            }
            else if (Request.QueryString["op3"] != null)
            {
                using (var db = new EntityFramework())
                {
                    var player = db.Players.Find(Int32.Parse(ids[3]));

                    string[] date = player.playerBirthdate.ToString().Split(' ');
                    tbDate.Text = date[0];
                    tbNombre.Text = player.playerName;
                    tbLastname.Text = player.playerName;
                    
                    if (player.playerImg != null)
                    {
                        string imreBase64Data = Convert.ToBase64String(player.playerImg);
                        string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                        imgCrud.Src = imgDataURL;
                    }
                }

            }
            else
            {
                using (var db = new EntityFramework())
                {
                    var league = db.Leagues.Find(Int32.Parse(ids[1]));
                    tbNombre.Text = league.leagueName;
                    if (league.leagueImg != null)
                    {
                        string imreBase64Data = Convert.ToBase64String(league.leagueImg);
                        string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                        imgCrud.Src = imgDataURL;
                    }
                }
            }
        }

        protected void BtnCreateClick(object sender, EventArgs e)
        {
            byte[] bytes = null;
            string[] userId = Session["usuarioLogueado"].ToString().Split('-');

            if (Request.QueryString["op2"] != null)
            {
                sOpc2 = Request.QueryString["op2"].ToString();
                string[] leagueId = sOpc2.Split('-');

                if (FileUpload.HasFile)
                {
                    int length = FileUpload.PostedFile.ContentLength;
                    bytes = new byte[length];
                    FileUpload.PostedFile.InputStream.Read(bytes, 0, length);

                }

                using (var db = new EntityFramework())
                {
                    db.Teams.Add(new Team { teamName = tbNombre.Text, team_LeagueId = Int32.Parse(leagueId[1]), });
                    db.SaveChanges();
                }

            } else if (Request.QueryString["op3"] != null) {


                string date = tbDate.Text;
                DateTime dt = DateTime.ParseExact(date, "yyyy-MM-dd", null);

                if (FileUpload.HasFile)
                {
                    int length = FileUpload.PostedFile.ContentLength;
                    bytes = new byte[length];
                    FileUpload.PostedFile.InputStream.Read(bytes, 0, length);

                }

                sOpc3 = Request.QueryString["op3"].ToString();
                string[] teamId = sOpc3.Split('-');
                using (var db = new EntityFramework())
                {
                    db.Players.Add(new Player { playerName = tbNombre.Text, playerLastName = tbLastname.Text,
                       player_TeamId = Int32.Parse(teamId[2]), playerImg = bytes, playerBirthdate = dt});
                    db.SaveChanges();
                }

            }else {

                

                if (FileUpload.HasFile)
                {
                    int length = FileUpload.PostedFile.ContentLength;
                    bytes = new byte[length];
                    FileUpload.PostedFile.InputStream.Read(bytes, 0, length);
                }

                using (var db = new EntityFramework())
                {
                    db.Leagues.Add(new League { leagueName = tbNombre.Text, league_UserId = Int32.Parse(userId[1]), leagueImg = bytes });
                    db.SaveChanges();
                }

                Response.Redirect("Index.aspx");
            }
        }

        protected void BtnUpdateClick(object sender, EventArgs e)
        {
            byte[] bytes = null;
            string[] ids = sID.Split('-');
            if (Request.QueryString["op2"] != null)
            {

                if (FileUpload.HasFile)
                {
                    int length = FileUpload.PostedFile.ContentLength;
                    bytes = new byte[length];
                    FileUpload.PostedFile.InputStream.Read(bytes, 0, length);

                }

                using (var db = new EntityFramework())
                {
                    var team = db.Teams.Find(Int32.Parse(ids[2]));
                    team.teamImg = bytes;
                    team.teamName = tbNombre.Text;
                    team.team_LeagueId = Int32.Parse(ids[1]);
                    db.SaveChanges();
                }

            }
            else if (Request.QueryString["op3"] != null)
            {

                
                string date = tbDate.Text+" 0:00:00";
                DateTime dt = DateTime.ParseExact(date, "yyyy-MM-dd", null);

                if (FileUpload.HasFile)
                {
                    int length = FileUpload.PostedFile.ContentLength;
                    bytes = new byte[length];
                    FileUpload.PostedFile.InputStream.Read(bytes, 0, length);
                    
                }

                using (var db = new EntityFramework())
                {
                    var player = db.Players.Find(Int32.Parse(ids[3]));
                    player.playerImg = bytes;
                    player.playerName = tbNombre.Text;
                    player.playerLastName = tbNombre.Text;
                    player.playerBirthdate = dt;
                    player.player_TeamId = Int32.Parse(ids[2]);
                    db.SaveChanges();
                }

            }
            else
            {
                if (FileUpload.HasFile)
                {
                    int length = FileUpload.PostedFile.ContentLength;
                    bytes = new byte[length];
                    FileUpload.PostedFile.InputStream.Read(bytes, 0, length);

                }

                using (var db = new EntityFramework())
                {
                    var league = db.Leagues.Find(Int32.Parse(ids[1]));
                    league.leagueImg = bytes;
                    league.leagueName = tbNombre.Text;
                    league.league_UserId = Int32.Parse(ids[0]);
                    db.SaveChanges();
                }

                Response.Redirect("Index.aspx");
            }
        }

        /*protected void BtnDeleteClick(object sender, EventArgs e)
        {
            string[] ids = sID.Split('-');

            if (Request.QueryString["op2"] != null)
            {
                using (var db = new EntityFramework())
                {
                    var team = db.Teams.Find(ids[2]);
                    db.Entry(team).State = EntityState.Deleted;
                    db.SaveChanges();
                }

            }
            else if (Request.QueryString["op3"] != null)
            {
                using (var db = new EntityFramework())
                {
                    var player = db.Players.Find(ids[3]);
                    db.Entry(player).State = EntityState.Deleted;
                    db.SaveChanges();
                }

            }
            else
            {
                using (var db = new EntityFramework())
                {
                    var league = db.Leagues.Find(ids[1]);
                    db.Entry(league).State = EntityState.Deleted;
                    db.SaveChanges();
                }

            }
            Response.Redirect(Request.RawUrl);
        }*/
    }
}