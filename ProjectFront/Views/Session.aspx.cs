﻿using ProjectFront.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace ProjectFront.Pages
{
    public partial class Session : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["usuarioLogueado"] != null)
            {
                string[] userId = Session["usuarioLogueado"].ToString().Split('-');
                sessionId.InnerText = userId[0];
                SessionUser.Visible = false;
                BtnIngresar.Visible = false;
                BtnLogout.Visible = true;
                BtnRegister.Visible = false;
                tbUser.Visible = false;
                tbPass.Visible = false;
                lbPass.Visible = false;
                FileUpload2.Visible = true;
                lbImg.Visible = true;
                btnImg.Visible = true;
                sessionImg.Visible = true;

                using (var db = new EntityFramework())
                {
                    var user = db.Users.Find(Int32.Parse(userId[1]));
                    if (user.userImg!=null) {
                        string imreBase64Data = Convert.ToBase64String(user.userImg);
                        string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                        imgUser.Src = imgDataURL;
                    }
                }
            }
            else
            {
                sessionId.InnerText = "Login";
                BtnLogout.Visible = false;
            }
        }

        protected void BtnDesconectarClick(object sender, EventArgs e)
        {
            Session["usuarioLogueado"] = null;
            Response.Redirect("Index.aspx");
            HttpContext.Current.Session.Abandon();
        }

        protected void BtnIngresarClick(object sender, EventArgs e) 
        {
            string user = tbUser.Text;
            string pass = tbPass.Text;

            var values = AutenticarUsuario(user, pass);
            var isUser = values.isUser;
            var userId = values.userId;

            if (isUser)
            {
                Session["usuarioLogueado"] = tbUser.Text + "-"+ userId;
                Response.Redirect("Index.aspx");
            }
            else {
                lbMessage.Visible = true;
                lbMessage.Text = "Wrong User/Pass";
                lbMessage.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void BtnRegistrarseClick(object sender, EventArgs e)
        {
            Response.Redirect("Register.aspx");
        }

        protected void BtnCambiarImgClick(object sender, EventArgs e)
        {
            lbMessage.Visible = true;

            if (FileUpload2.HasFile) {
                //FileUpload2.SaveAs(Server.MapPath("~/Uploads/" + FileUpload2.FileName));
                lbMessage.Text = "Uploaded";

                /*HttpPostedFile postedFile = FileUpload2.PostedFile;
                Stream stream = postedFile.InputStream;
                BinaryReader binaryReader = new BinaryReader(stream);
                byte[] bytes = binaryReader.ReadBytes((int)stream.Length);*/

                int length = FileUpload2.PostedFile.ContentLength;
                byte[] bytes = new byte[length];
                FileUpload2.PostedFile.InputStream.Read(bytes, 0, length);
                string[] userId = Session["usuarioLogueado"].ToString().Split('-');

                using (var db = new EntityFramework())
                {
                    var user = db.Users.Find(Int32.Parse(userId[1]));
                    user.userImg = bytes;
                    db.SaveChanges();
                }               
            }else{

                lbMessage.Text = "select a file";
            }

        }

        private (bool isUser, string userId) AutenticarUsuario(string user, string pass)
        {
            bool isUser = false;
            string userId = "";
            using (var context = new EntityFramework())
            {
                List<User> users = context.Users.ToList();

                foreach (var User in users)
                {
                    if (User.userName==user.ToLower() && User.userPass==pass)
                    {
                        isUser=true;
                        userId= User.userId.ToString(); 
                        break;
                    }
                }
            }
            return (isUser,userId);
        }
    }
}