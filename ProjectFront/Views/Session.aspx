﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.Master" AutoEventWireup="true" CodeBehind="Session.aspx.cs" Inherits="ProjectFront.Pages.Session" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <hr>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 mx-auto">
                <div class="card justify-content-start" style="width: 18rem;">
                    <div class="card-body">
                             <div class="row justify-content-center mb-3">
                                <div class="col">
                                <img runat="server" id="imgUser" class="rounded-circle" src="~/Content/login.jpg" width="255" height="255">
                                </div>
                              </div>
                        <div class="row">
                            <div class="col">
                                <center>
                                    <h3 id="sessionId" runat="server"></h3>
                                </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <Form id="form" runat="server">
                                <div class="col">
                                    <asp:Label runat="server" ID="SessionUser" Text="User Id"></asp:Label>
                                    <div class="form-group">
                                        <asp:TextBox CssClass="form-control" ID="tbUser" runat="server" placeholder="Your ID"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col">
                                    <asp:Label runat="server" ID="lbPass">Password</asp:Label>
                                    <div class="form-group">
                                        <asp:TextBox CssClass="form-control" ID="tbPass" runat="server" placeholder="Your Password" TextMode="Password"></asp:TextBox>
                                        <asp:Label id="lbMessage" Text="" runat="server" Visible="false" />
                                        <br />
                                    </div>
                                </div>
                                <div class="col">
    <div>
       <h4 id="sessionImg" runat="server" Text="Cambiar Imagen:" Visible="false"></h4>
   
       <asp:FileUpload id="FileUpload2"                 
           runat="server" Visible="false">
       </asp:FileUpload>
       <br /><br />
       <asp:Button CssClass="btn btn-success" id="btnImg" 
           Text="Cambiar"
           OnClick="BtnCambiarImgClick"
           runat="server" Visible="false">
       </asp:Button>    
       <hr />
       <asp:Label id="lbImg"
           runat="server" Visible="false">
       </asp:Label>        
    </div>
                                </div>
                                <asp:Button id="BtnLogout" CssClass="btn btn-danger" runat="server" type="submit" class="btn btn-primary" Text="Logout" OnClick="BtnDesconectarClick"></asp:Button>
                                <asp:Button id="BtnIngresar" CssClass="btn btn-primary" runat="server" type="submit" class="btn btn-primary" Text="Login" OnClick="BtnIngresarClick"></asp:Button>
                                <asp:Button runat="server" Text="Register" CssClass="btn form-control-sm btn-info" ID="BtnRegister" OnClick="BtnRegistrarseClick"/>
                                <asp:Button id="backButton" runat="server" CssClass="btn btn-dark" Text="Volver" OnClientClick="JavaScript:window.history.back(1);return false;"></asp:Button>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
