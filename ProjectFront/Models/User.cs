﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectFront.Models
{
    public class User
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column("Id")]
        public int userId { get; set; }

        [Required, StringLength(60), Column("Name")]
        public string userName { get; set; }

        [Required, StringLength(60), Column("Pass")]
        public string userPass { get; set; }

        [Required, StringLength(60), Column("Type")]
        public string userType { get; set; }

        [Column("Logo")]
        public byte[] userImg { get; set; }

        public virtual ICollection<League> Leagues { get; set; }
    }
}