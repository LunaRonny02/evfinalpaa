﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectFront.Models
{
    public class Player
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column("Id")]
        public int playerId { get; set; }

        public int player_TeamId { get; set; }

        public Team Team { get; set; }

        [Required, StringLength(25), Column("Nombre")]
        public string playerName { get; set; }

        [Required, StringLength(25), Column("Apellido")]
        public string playerLastName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Birthdate")]
        public DateTime playerBirthdate { get; set; }

        [Column("Logo")]
        public byte[] playerImg { get; set; }

    }
}