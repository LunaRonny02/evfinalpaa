﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectFront.Models
{
    public class Team
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column("Id")]
        public int teamId { get; set; }

        public int team_LeagueId { get; set; }

        public League League { get; set; }

        [Required, StringLength(60), Column("Nombre")]
        public string teamName { get; set; }

        [Column("Logo")]
        public byte[] teamImg { get; set; }

        public virtual ICollection<Player> Players { get; set; }
    }
}