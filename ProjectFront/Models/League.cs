﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectFront.Models
{
    public class League
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column("Id")]
        public int leagueId { get; set; }

        public int league_UserId { get; set; }

        public User User { get; set; }

        [Required, StringLength(60), Column("Nombre")]
        public string leagueName { get; set; }

        [Column("Logo")]
        public byte[] leagueImg { get; set; }

        public virtual ICollection<Team> Teams { get; set; }

    }
}